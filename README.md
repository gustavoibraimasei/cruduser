# CrudUser

Teste técnico com angular e .netcore

Fazer clone do projeto no diretório C:

git clone https://gitlab.com/dongustavosccp/cruduser.git




Executar os dois comandos abaixo no terminal(cmd do windows) no Diretorio C:\CrudUser\CrudUser.Api\CrudUser.Api

dotnet ef --startup-project  "C:\CrudUser\CrudUser.Api\CrudUser.Api\CrudUser.WebApi\CrudUser.Api.WebApi.csproj" migrations add v1
dotnet ef --startup-project  "C:\CrudUser\CrudUser.Api\CrudUser.Api\CrudUser.WebApi\CrudUser.Api.WebApi.csproj" database update

esses comandos acima irão criar a base de dados no sql server


Verificar se a instalação do SQL Server está conforme o app.setting "DefaultConnection": "Server=localhost;Database=Usuario;Trusted_Connection=True;User Id=admin;Password=Teste@2020@;MultipleActiveResultSets=true"


Comandos para adicionar a escolaridade
USE Usuario
GO
INSERT TB_ESCOLARIDADE
(
	Descricao
)
VALUES
(
	'Infantil'
),
(
	'Fundamental'
)
,
(
	'Médio'
),
(
	'Superior'
)
