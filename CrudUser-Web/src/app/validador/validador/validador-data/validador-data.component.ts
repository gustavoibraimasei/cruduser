import { AbstractControl } from '@angular/forms';
import moment from 'moment';

export function validadorData(control: AbstractControl) {
  const dataAtual = new Date().getTime();
  const dataFormatada = control.value.split('/');
  const data = new Date(
    dataFormatada[2],
    dataFormatada[1] - 1,
    dataFormatada[0]
  );

  if (!moment(control.value, 'DD/MM/YYYY', true).isValid()) {
    return { validadorData: true };
  }

  if (data.getTime() > dataAtual) {
    return { validadorData: true };
  }

  return null;
}
