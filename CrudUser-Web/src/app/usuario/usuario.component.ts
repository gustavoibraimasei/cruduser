import { Component, OnInit } from '@angular/core';
import {Usuario, UsuarioApresentacao} from './usuario';
import {UsuarioService} from './usuario.service';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {
  usuarios: Observable<UsuarioApresentacao[]>;
  UsuarioService: any;
  usuario: Usuario;

  constructor(private usuarioService: UsuarioService) { }

  ngOnInit(): void {
    this.obterUsuarios();
  }

  obterUsuarios() {
    this.usuarios = this.usuarioService.obterUsuarios();
  }
  
  obterUsuarioporId(id: number) {
    this.usuario = this.usuarioService.obterUsuarioPorId(usuarioId);
  }
  
  excluirUsuarioPorId(usuarioid: number){
    this.usuarioService.excluirUsuarioPorId(usuarioid).subscribe(this.ngOnInit());
  }

}
