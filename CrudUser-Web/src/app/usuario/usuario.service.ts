import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable} from 'rxjs';
import { Usuario, UsuarioApresentacao } from './usuario';
import { map } from "rxjs/operators";
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};


@Injectable({
  providedIn: 'root',
})

export class UsuarioService {
  url = 'https://localhost:44336/api/Usuarios';
  constructor(private http: HttpClient) {}

  obterUsuarios(): Observable<UsuarioApresentacao[]> {
    return this.http.get<UsuarioApresentacao[]>(`${this.url}/ObterTodosUsuarios`).pipe(map(res => res.retorno));
  }

  salvarUsuario(usuario: Usuario) {
    const body = JSON.stringify(usuario);
    return this.http.post(`${this.url}/SalvarUsuario`, body, httpOptions).subscribe(res => console.log(res));
  }

  excluirUsuarioPorId(usuarioid: number) {
    const apiurl = `${this.url}/ExcluirUsuario/${usuarioid}`;
    return this.http.delete(apiurl, httpOptions).subscribe(res => console.log(res));
  }

  obterUsuarioPorId(usuarioid: number) {
    const apiurl = `${this.url}/ObterUsuarioPorId/${usuarioid}`;
    return this.http.get(apiurl, httpOptions).pipe(map(res => res.retorno));
  }
}
