import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Usuario } from '../usuario/usuario';
import { UsuarioService } from '../usuario/usuario.service';
import { DatePipe } from '@angular/common';
import { validadorData } from '../validador/validador/validador-data/validador-data.component';
import { EscolaridadeService } from '../escolaridade/escolaridade.service';
import { Escolaridade } from '../escolaridade/escolaridade';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-usuario-cadastro',
  templateUrl: './usuario-cadastro.component.html',
  styleUrls: ['./usuario-cadastro.component.css'],
})


export class UsuarioCadastroComponent implements OnInit {

  form: FormGroup;
  actionType: string;
  formNome: string;
  formSobrenome: string;
  formEmail: string;
  formDataNascimento: string;
  formEscolaridade: string;
  id: number;
  errorMessage: any;
  usuarioCarregado: Usuario;
  dataNascimentoEnvio: Date;
  escolaridadeLista: Observable<Escolaridade[]>;

  constructor(
    private usuarioService: UsuarioService,
    private escolaridadeService: EscolaridadeService,
    private formBuilder: FormBuilder,
    private avRoute: ActivatedRoute,
    private router: Router,
    private datePipe: DatePipe
  ) {
    const idParam = 'id';
    this.actionType = 'Adicionar';
    this.formNome = 'nome';
    this.formSobrenome = 'sobrenome';
    this.formEmail = 'email';
    this.formDataNascimento = 'dataNascimento';
    this.formEscolaridade = 'escolaridade';
    if (this.avRoute.snapshot.params[idParam]) {
      this.id = this.avRoute.snapshot.params[idParam];
    }


    this.form = this.formBuilder.group({
      id: 0,
      nome: ['', [Validators.required]],
      sobrenome: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      dataNascimento: ['', [Validators.required, validadorData]],
      escolaridade: ['', [Validators.required],
    });
  }

  obterEscolaridade() {
    this.escolaridadeLista = this.escolaridadeService.obterEscolaridades();
  }

  ngOnInit(): void {

    this.obterEscolaridade();

    if (this.id > 0) {
      this.actionType = 'Editar';
      this.usuarioService
        .obterUsuarioPorId(this.id)
        .subscribe(
          (data) => (
            (this.usuarioCarregado = data),
            this.form.controls[this.formNome].setValue(data.nome),
            this.form.controls[this.formSobrenome].setValue(data.sobrenome),
            this.form.controls[this.formDataNascimento].setValue(this.datePipe.transform(data.dataNascimento, 'dd/MM/yyyy')),
            this.form.controls[this.formEmail].setValue(data.email),
            this.form.controls[this.formEscolaridade].setValue(data.idEscolaridade)
          )
        );
    }
  }

  salvarUsuario() {
    let usuario: Usuario;
    const dataFormatada = this.form.get(this.formDataNascimento).value.split('/');

    this.dataNascimentoEnvio = new Date(dataFormatada[2], dataFormatada[1] - 1, dataFormatada[0]);

    if (!this.form.valid) {
      return;
    }

    if (this.actionType === 'Adicionar') {
      usuario = {
        nome: this.form.get(this.formNome).value,
        sobrenome: this.form.get(this.formSobrenome).value,
        id: 0,
        email: this.form.get(this.formEmail).value,
        dataNascimento: this.dataNascimentoEnvio,
        idEscolaridade: this.form.get(this.formEscolaridade).value
      };
    } else {
      usuario = {
        nome: this.form.get(this.formNome).value,
        sobrenome: this.form.get(this.formSobrenome).value,
        id: this.usuarioCarregado.id,
        email: this.form.get(this.formEmail).value,
        dataNascimento: this.dataNascimentoEnvio,
        idEscolaridade: this.form.get(this.formEscolaridade).value,
      };
    }

    this.usuarioService.salvarUsuario(usuario);
    this.cancelar();
  }

  cancelar() {
    
    this.router.navigate(['/usuario']);
  }

  get nome() {
    return this.form.get(this.formNome);
  }
  get sobrenome() {
    return this.form.get(this.formSobrenome);
  }

  get email()
  {
    return this.form.get(this.formEmail);
  }

  get dataNascimento()
  {
    return this.form.get(this.formDataNascimento);
  }

  get escolaridade()
  {
    return this.form.get(this.formEscolaridade);
  }
}
