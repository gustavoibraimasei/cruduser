﻿using CrudUser.Api.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CrudUser.Api.Data.Mapping
{
    public class UsuarioMap : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.ToTable("TB_USUARIO");

            //Definindo chave primária
            builder.HasKey(p => p.Id);

            //Definindo relacionamento com escolaridade
            builder.HasOne(u => u.Escolaridades).WithMany(a => a.Usuario).HasForeignKey(u => u.IdEscolaridade);

            //Definindo Propriedades
            builder.Property(p => p.Id);
            builder.Property(p => p.Nome);
            builder.Property(p => p.Sobrenome);
            builder.Property(p => p.Email);
            builder.Property(p => p.DataNascimento);
            builder.Property(p => p.IdEscolaridade).HasColumnName("IdEscolaridade");
        }

    }
}
