﻿using CrudUser.Api.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CrudUser.Api.Data.Mapping
{
    public class EscolaridadeMap : IEntityTypeConfiguration<Escolaridade>
    {
        public void Configure(EntityTypeBuilder<Escolaridade> builder)

        {
            builder.ToTable("TB_ESCOLARIDADE");

            //Definindo chave primária
            builder.HasKey(p => p.Id);

            //Definindo Propriedades
            builder.Property(p => p.Id);
            builder.Property(p => p.Descricao);
        }

    }
}