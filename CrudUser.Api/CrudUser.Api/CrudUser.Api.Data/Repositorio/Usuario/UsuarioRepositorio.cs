﻿using CrudUser.Api.Data.Context;
using CrudUser.Api.Domain;
using CrudUser.Api.Domain.Contratos.Repositorio;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace CrudUser.Api.Data.Repositorio
{
    public class UsuarioRepositorio : IUsuarioRepositorio
    {
        private ApplicationDbContext _context;

        public UsuarioRepositorio(ApplicationDbContext context)
        {
            _context = context;
        }

        public void ExcluirUsuario(int id)
        {
            Usuario usuario = ObterUsuarioPorId(id);
            if (usuario != null)
            {
                _context.Remove(usuario);
                GravarAlteracoes();
            }
        }

        public IEnumerable<Usuario> ObterTodosUsuarios()
        {
            return _context.Usuarios.Include(e => e.Escolaridades).ToList();
        }

        public Usuario ObterUsuarioPorId(int id)
        {
            return _context.Usuarios.Where(u => u.Id == id).FirstOrDefault();
        }

        public void Salvar(Usuario usuario)
        {
            if (usuario.Id > 0)
            {
                _context.Update(usuario);
            }
            else
            {
                _context.Add(usuario);
            }

            GravarAlteracoes();
        }

        public void GravarAlteracoes()
        {
            _context.SaveChanges();
        }
    }
}
