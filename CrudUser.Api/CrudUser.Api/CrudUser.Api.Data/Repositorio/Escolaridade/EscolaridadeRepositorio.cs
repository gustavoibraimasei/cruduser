﻿using System.Collections.Generic;
using System.Linq;
using CrudUser.Api.Data.Context;
using CrudUser.Api.Domain;
using CrudUser.Api.Domain.Contratos.Repositorio;

namespace CrudUser.Api.Data.Repositorio
{
    public class EscolaridadeRepositorio : IEscolaridadeRepositorio
    {
        private ApplicationDbContext _context;

        public EscolaridadeRepositorio(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Escolaridade> ObterTodasEscolaridades()
        {
            return _context.Escolaridades.ToList();
        }
    }
}
