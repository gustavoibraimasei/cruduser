﻿using Newtonsoft.Json;
using System.Collections.Generic;
namespace CrudUser.Api.Domain
{
    public class Escolaridade
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public virtual ICollection<Usuario> Usuario { get; set; }
    }
}
