﻿using System;
using System.Collections.Generic;
using CrudUser.Api.Domain;

namespace CrudUser.Api.Domain
{
    public class Usuario
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Sobrenome { get; set; }
        public string Email { get; set; }
        public DateTime DataNascimento { get; set; }
        public int IdEscolaridade { get; set; }
        public virtual Escolaridade Escolaridades { get; set; }
    }
}
