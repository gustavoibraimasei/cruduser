﻿using CrudUser.Api.Domain.DomainNotification;
using System;
using System.Collections.Generic;
using System.Text;

namespace CrudUser.Api.Domain.Contratos.DomainNotification
{
    public interface INotificador
    {
        bool TemNotificacao();
        List<Notificacao> ObterNotificacoes();
        void AdicionarNotificacao(Notificacao notificacao);
    }
}
