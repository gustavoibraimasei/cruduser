﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrudUser.Api.Domain.Contratos.Repositorio
{
    public interface IUsuarioRepositorio
    {
        void Salvar(Usuario usuario);
        Usuario ObterUsuarioPorId(int id);
        IEnumerable<Usuario> ObterTodosUsuarios();
        void ExcluirUsuario(int id);
    }
}
