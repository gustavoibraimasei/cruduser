﻿using System.Collections.Generic;

namespace CrudUser.Api.Domain.Contratos.Repositorio
{
    public interface IEscolaridadeRepositorio
    {
        IEnumerable<Escolaridade> ObterTodasEscolaridades();
    }
}
