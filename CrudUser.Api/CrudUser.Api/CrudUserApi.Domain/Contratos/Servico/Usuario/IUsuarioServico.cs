﻿using System.Collections.Generic;

namespace CrudUser.Api.Domain.Contratos.Servico
{
    public interface IUsuarioServico
    {
        void Salvar(Usuario usuario);
        Usuario ObterUsuarioPorId(int id);
        IEnumerable<UsuarioViewModel> ObterTodosUsuarios();
        void ExcluirUsuario(int id);
    }
}
