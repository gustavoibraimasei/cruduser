﻿using CrudUser.Api.Data.Context;
using CrudUser.Api.Data.Repositorio;
using CrudUser.Api.Domain.Contratos.DomainNotification;
using CrudUser.Api.Domain.Contratos.Repositorio;
using CrudUser.Api.Domain.Contratos.Servico;
using CrudUser.Api.Domain.DomainNotification;
using CrudUser.Api.Servico;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CrudUser.Api.WebApi.Config
{
    public static class DependencyInjectionConfig
    {
        //<summary>
        //Responsavel por registrar as injeções de dependencia na Startup
        //</summary>
        public static IServiceCollection ResolveDependency(this IServiceCollection services, IConfiguration configuration = null)
        {
            //Inicio a configuração da base de dados para o entity framework core
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
           
            //Notificação de domínio
            services.AddScoped<INotificador, Notificador>();

            // Registrar injeção de dependencia
            services.AddScoped<IUsuarioRepositorio, UsuarioRepositorio>();
            services.AddScoped<IUsuarioServico, UsuarioServico>();

            services.AddScoped<IEscolaridadeRepositorio, EscolaridadeRepositorio>();
            services.AddScoped<IEscolaridadeServico, EscolaridadeServico>();

            if (configuration != null)
                services.AddSingleton(configuration);

            return services;
        }
    }
}
