﻿using CrudUser.Api.Domain.Contratos.DomainNotification;
using CrudUser.Api.Domain.Contratos.Servico;
using CrudUser.Api.WebApi.Controllers.Base;
using CrudUser.Api.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace CrudUser.Api.WebApi.Controllers.Usuarios
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : BaseController<dynamic>
    {
        private readonly IUsuarioServico _usuarioServico;
        private readonly IConfiguration _configuration;

        public UsuariosController(IUsuarioServico usuarioServico, IConfiguration configuration, INotificador notificador) : base(notificador)
        {
            _usuarioServico = usuarioServico;
            _configuration = configuration;
        }

        [HttpGet("ObterTodosUsuarios")]
        public ActionResult<IEnumerable<Usuario>> ObterTodosUsuarios()
        {
            IEnumerable<UsuarioViewModel> retorno = null;

            try
            {
                retorno = _usuarioServico.ObterTodosUsuarios();
            }
            catch (System.Exception)
            {
                NotificarErro("Erro ao consultar a lista de usuários");
            }

            return CustomResponse(retorno);
        }

        [HttpGet("ObterUsuarioPorId/{id}")]
        public ActionResult<IEnumerable<Usuario>> ObterUsuarioPorId(int id)
        {
           Usuario retorno = null;

            try
            {
                retorno = _usuarioServico.ObterUsuarioPorId(id);
            }
            catch (System.Exception)
            {
                NotificarErro("Erro ao consultar a lista de usuários");
            }

            return CustomResponse(retorno);
        }

        [HttpPost("SalvarUsuario")]
        public void SalvarUsuario(Usuario usuario)
        {
            try
            {
                _usuarioServico.Salvar(usuario);
            }
            catch (System.Exception)
            {
                NotificarErro("Erro ao salvar o usuário");
            }
        }

        [HttpDelete("ExcluirUsuario/{id}")]
        public void ExcluirUsuario(int id)
        {
            try
            {
                _usuarioServico.ExcluirUsuario(id);
            }
            catch (System.Exception)
            {

                NotificarErro("Erro ao salvar o usuário");
            }
        }
    }
}